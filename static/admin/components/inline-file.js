const getIcon = (fileType) => {
    switch (fileType) {
      case 'pdf': 
        return 'file-pdf';
      default:
        return "file-document";
    }
}

export default {
    // Internal id of the component
    id: "inline-file",
    // Visible label
    label: "File",
    // Fields the user need to fill o ut when adding an instance of the component
    fields: [
      {
        name: 'label',
        label: 'Text to be displayed',
        widget: 'string'
      },
      {
        name: 'file',
        label: 'File',
        widget: 'file'
      }
    ],
    // Regex pattern used to search for instances of this block in the markdown document.
    pattern: /\[{{<\s(mdi)\s(.*)\s>}}(.*)\]\((.*)(\..*)\)/,
    // This is used to populate the custom widget in the markdown editor in the CMS.
    fromBlock: function(match) {
      return {
        label: match[3],
        file: match[4] + match[5],
        fileType: match[5],
      };
    },
    // This is used to serialize the data from the custom widget to the
    // markdown document
    toBlock: function(data) {
      return `[{{< mdi ${getIcon(data.fileType)} >}} ${data.label}](${data.file})`;
    },
    // Preview output for this component. Can either be a string or a React component
    // (component gives better render performance)
    toPreview: function(data) {
      return `<a href="${data.file}"> <i class="mdi mdi-${getIcon(data.fileType)}"></i> ${data.label}</a>`;
    }
  }