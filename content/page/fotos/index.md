---
title : Fotos
bigimg: []
---

Her er en samling af fotos, som vores medlemmer har taget.

{{< gallery >}}
  {{< figure src="/page/fotos/images/Hintertux-(10).jpg" title="Geforenere wand">}}
  {{< figure src="/page/fotos/images/Vinterferie-078.jfif" title="Toppen af Hintertux gletcheren">}}
{{< /gallery >}}
