---
title : Langrend
bigimg: [{src: "../../page-images/langrend.jpg", desc: ""}]
---

Langrend er lidt et forsømt område for klubben. Vi har ambitionerne om at gøre mere ved dette, bla. vil vi gerne have gang i noget omkring rulleski. Tiltagene strander hver gang på manglen på frivillige med indsigt i sporten og som vil stå for træningen. Er det noget for dig må du endelig kontakte os, det kan du gøre i menuen øverst til højre. Det tilbud vi har er at Silkeborg Golf Clubs 112 hektar står til rådighed for langrendsløbere, når vilkårene for skiløb tillader det. Endvidere er alle velkommen i klubhuset, der både kan tilbyde toiletter og varm chokolade.

Når der ligger tilstrækkeligt med sne, starter golfklubbens greenkeepere snescooteren og præparerer et net af langrendsspor overalt på banen.

En golfbane er imidlertid sårbar overfor skiløb, og der henstilles derfor til, at alle over følgende etiketteregler:
- Løb venligst kun i de præparerede spor.
- Det er strengt forbudt at overskride afspærringer, da greens kan tage alvorlig skade.
- Hunde skal føres i snor.
- Toiletter og vandposter på banen er lukkede.
- Alle er velkommen til at benytte golfklubbens P-pladser samt toiletterne i klubhuset.
- Alle er naturligvis også velkommen i Restaurant Golfklubben til 'after-ski'.
- Endelig vil golfklubben appellere til ikke-skiløberne:
- GÅENDE BEDES VENLIGST IKKE TRÆDE I SKISPORENE