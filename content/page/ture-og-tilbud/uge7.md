---
title: Uge 7 tur
bigimg:
  - src: ../../page-images/uge7.jpg
    desc: ""
---
[{{< mdi file-pdf>}} Invitation 2021](/attachments/InvitationUge_7_2021.doc)

Uge 7 turen er absolut klubbens flagskib. Turen går hvert år til Vorderlanersbach i Zillertal, hvor vi bor på hotel Kirchlerhof. Der er mulighed for både kør selv eller tage med bussen fra Sørens Rejser. Det er en tur der henvender sig til alpinisterne. Der er dog mulighed for lidt langrend på en 7 km ud-hjem løjpe, der ligger lige bag det hotel vi bor på.

Den kommende tur vil muligvis være præget af coronasituationen. Læs derfor bla. nedenstående Corona afsnit.

## Corona

Vi er alle i en tid, som vi ikke tidligere har været i. Vi fik efter hjemkomsten fra uge 7 turen i år besøg af Corona i Danmark og hele verden. Det har forandret vores adfærd generelt og det vil naturligvis også få betydning for vores uge 7 tur i 2021. Vi vil i bestyrelsen for Silkeborg Skiklub gøre alt for, at vores kommende uge 7 tur bliver en oplevelsesrig tur med de restriktioner, som Coronaen vil kræve. Og der vil kunne opstå situationer i forhold til Corona, som vi p.t. ikke kan forudse. Kommer der ændringer i forhold til turen, vil I straks blive informeret. Og har I spørgsmål, er I meget velkommen til at kontakte os - brug gerne ovenstående mailadresse.

### Bussen

Vælger du at køre med bus, vil der være restriktioner omkring brug af mundbind. P.t. kan alle sæder i bussen anvendes.

### Hotellet

Vi er endnu ikke bekendt med restriktioner på hotellet.

### Rejseforsikring

I skal undersøge, hvordan I er stillet forsikringsmæssigt, hvis turen bliver aflyst pga. restriktioner i forbindelse med Corona 

### Silkeborg Skiklub

Hvis turen skulle blive aflyst pga. Corona, vil vi gøre alt for, at I får refunderet så meget som muligt af turens pris. Vi kan desværre ikke love, at der vil kunne ske en refusion. Derfor er det vigtigt, at I er dækket forsikringsmæssigt.
