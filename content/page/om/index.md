---
title: Om Silkeborg Skiklub
bigimg: []
---
[{{< mdi file-document >}} Vedtægter](/attachments/2016_Vedtægter.pdf)
[{{< mdi file-document >}} Persondatapolitik](/attachments/20180820_Persondatapolitik_SSK.pdf)

Foreningens navn er Silkeborg Skiklub (SSK). Dens hjemsted er Silkeborg kommune. Foreningen er stiftet 1941 og er medlem af Danmarks Skiforbund (DSKIF) under Dansk Idrætsforbund (DIF) og Féderation Internationale de Ski (FIS), og underkastet disse forbunds love og vedtægter.

Foreningens formål er at udbrede interessen for skisport i Silkeborg og omegn.

At skabe de bedste muligheder for at dyrke sporten ved at arrangere rejser, herunder undervisning, konkurrencer og klubaftener.

Foreningen har i vedtagen privatlivspolitik, beskrevet hvordan den vil leve op til persondata forordningen. Se politikken i menuen til venstre.