---
title : Bestyrelse
bigimg: []
---

Foreningen ledes af en bestyrelse på mindst 5 medlemmer, nemlig formanden, der vælges for 2 år ad gangen på ulige årstal og kasseren, der vælges for 2 år ad gangen på lige årstal, samt mindst 3 bestyrelsesmedlemmer, hvoraf 1 vælges for 2 år ad gangen på ulige årstal og 2 vælges for 2 år ad gangen på lige årstal. Genvalg kan finde sted.

For nuværende er bestyrelsen sammensat således:

| Rolle                 | Navn                                                            |
|-----------------------|:----------------------------------------------------------------|
| Formand               | [Steen Secher](mailto:formand@silkeborgskiklub.dk)              |
| Kasserer og webmaster | [Søren Damsgaard Andersen](mailto:kasserer@silkeborgskiklub.dk) |
| Ture og sponsorater   | [Bettina Hvidberg](mailto:bettina@silkeborgskiklub.dk)          |
| Tur økonomi           | [Jan Hvidberg](mailto:bettina@silkeborgskiklub.dk)              |
| Instruktører          | [Simon Probst](mailto:SIP@mdhs-probst.dk)                       |
| Medlemsregistrering   | [Helle Christensen](mailto:kasserer@silkeborgskiklub.dk)        |








