---
title : "Indmeldelse"
bigimg: []
---

Ønsker du at blive medlem af Silkeborg Skiklub vil vi bede dig udfylde og sende nedenstående formular, samt indbetale kontingent plus indmeldelsesgebyret til: **7170 0001298896**

Husk endelig at angive dit navn i beskedfeltet, så vi kan bogføre din indbetaling korrekt.

Kontingent satserne er som følger:

| Medlemstype                         | Kontingent pr. år | Indskud ved indmeldelse | Ved indmeldelse betales |
|-------------------------------------|:------------------| :-----------------------|:------------------------|
| Husstand                            | 375 kr            | 375 kr                  | 750 kr                  |
| Personlig - voksen 25 år og derover | 195 kr            | 195 kr                  | 390 kr                  |
| Personlig - unge 18-24 år           | 135 kr            | 135 kr                  | 270 kr                  |
| Personlig - under 18 år             | 85 kr             | 85 kr                   | 170 kr                  |

Kontingentet dækker for perioden 1. oktober til 30. september. Indmeldelse efter 1. juli dækker også den kommende kontingent periode. Kontingent for næste periode opkræves pr. 5. oktober.

{{< signupForm >}}
