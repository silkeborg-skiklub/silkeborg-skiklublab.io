# Silkeborg Skiklub Homepage
![Build Status](https://gitlab.com/silkeborg-skiklub/ssk-homepage/badges/master/pipeline.svg)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

  - [How to Write Markdown](#how-to-write-markdown)
  - [Folder Structure](#folder-structure)
    - [Static content](#static-content)
  - [Galleries](#galleries)
  - [Icons](#icons)
- [Hugo](#hugo)
  - [Building locally](#building-locally)
    - [Preview your site](#preview-your-site)
- [GitLab Pages](#gitlab-pages)
  - [Redeploying the Group Page](#redeploying-the-group-page)
  - [GitLab CI](#gitlab-ci)
  - [Did you fork this project?](#did-you-fork-this-project)
  - [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
Run `doctoc README.md` to regenerate the ToC (not `doctoc .`).

## How to Write Markdown
[Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Folder Structure
```
.
|-- content/            (Contains pages and posts content)
|-- layouts/            (Contains templates and shortcodes)
|-- static/             (Contains static content)
|-- |-- images/         
|-- |-- pdf/
|-- themes/
|-- .gitlab-ci.yml      (Gitlab CI configuration)
|-- .gitmodules
|-- config.toml         (Hugo configuration)
```
More information in the [Hugo Documentation - Directory Structure](https://gohugo.io/getting-started/directory-structure/)

### Static content
Static content such as images and pdfs belong in the ``static`` folder.
To reference an image from the static folder: ``/images/theimage.png``

Example:
```markdown
![Fitness](/images/ssk-photos/fitness2.jpg)
```
This will look for the image at the following development path: ``./static/images/ssk-photos/fitness2.jpg``

## Galleries
We are using [hugo-easy-gallery](https://github.com/liwenyip/hugo-easy-gallery) to create galleries.
To create a simple gallery, see the following example:

```
{{< gallery >}}
  {{< figure src="/images/ssk-photos/commonhouse/fælleshus.jpg" >}}
  {{< figure src="/images/ssk-photos/commonhouse/fælleshus2.jpg" >}}
  {{< figure src="/images/ssk-photos/commonhouse/fælleshus_boardgames.jpg" >}}
  {{< figure src="/images/ssk-photos/commonhouse/fælleshus_køkken.jpg" >}}
{{< /gallery >}}
```

More information can be seen on hugo-easy-gallery's [Github Page](https://github.com/liwenyip/hugo-easy-gallery)

## Icons

A shortcode has been created that makes it easy to insert icons in the text. We are using [MDI-Icons](http://code.meta-platform.com/assets/mdi/preview.html). A list of the available icons can be seen in the link.

To insert an icon in the text:

```
{{< mdi the-icon >}}
```

Where `the-icon` is replaced by the name of the icon without `mdi`. So if the icon is called `mdi-folder` in the list, you write:

```
{{< mdi folder >}}
```

---

# Hugo

We use Hugo to build our website. It, amongst other things, is responsible for converting the markdown files to html.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][] Hugo
3. Run: `git submodule update --init --recursive` to update all nested git-repos.
4. Preview your project: `hugo server`
5. Add content
6. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`, your site can by default be previewed under `localhost:1313`. The excact URL may vary depending on your PC, therefore, you should check your terminal for the excact URL after running `hugo server`.


# GitLab Pages

We use GitLab Pages to host the webpage.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## Redeploying the Group Page

The below is already done, but can be seen as documentation if you need to change it at some point.

To use this project as your group website, do the following: 

- Go to **Settings** -> **Advanced** -> **Change path** and change the path to `silkeborg-skiklub.gitlab.io`.

- You'll need to configure your site too: change this line
in your `config.toml`, to `baseurl = "https://silkeborg-skiklub.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).


Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means one of three things:

    1. You have wrongly set up the CSS URL in your templates
    2. Your static generator has a configuration option that needs to be explicitly set in order to serve static assets under a relative URL.
    3. The `baseurl` attribute in `config.toml` is wrong.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
